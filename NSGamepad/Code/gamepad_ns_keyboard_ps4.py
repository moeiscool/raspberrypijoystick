"""
Use a keyboard to control a PS4.

Remember to hit the PS4 LOGO button (keyboard key '0' ) to activate. The PS4
does not activate a controller until it sees the LOGO button press.
"""

import os
import sys
#import termios
import time
from NSGamepad import *

Gamepad = NSGamepad()
Gamepad.begin('/dev/hidg0')

# buttonKeys = ["a", "y", "x", "b", "l", "r", "zl", "zr", "l_stick", "r_stick", "plus"]
# buttonValues = ["CIRCLE", "SQUARE", "TRIANGLE", "CROSS", "L1", "R1", "L2", "R2", "L3", "R3", "OPTIONS"]

def getch():
    try:
        line = sys.stdin.readline().rstrip()
        eachCommand = line.split('&&')
        for ch in eachCommand :
            if ch.find('stick ') != -1:
                stringParts = ch.split(' ')
                stickChosen = stringParts[1] # left or right
                stickAxis = stringParts[2] # h or v
                stickPosition = int(stringParts[3]) # number between 0 and 255. 128 is center.
                scaledValue = int((stickPosition / 4094) * 255)
                if stickChosen == 'left':
                    if stickAxis == 'h':
                        Gamepad.leftXAxis(scaledValue)
                    else:
                        scaledValue = int(((scaledValue - 127.5) * -1) + 127.5)
                        Gamepad.leftYAxis(scaledValue)
                else:
                    if stickAxis == 'h':
                        Gamepad.rightXAxis(scaledValue)
                    else:
                        scaledValue = int(((scaledValue - 127.5) * -1) + 127.5)
                        Gamepad.rightYAxis(scaledValue)
            elif ch.find('hold') != -1:
                stringParts = ch.split(' ')
                button = stringParts[1] + '' # see buttonMap
                if button == 'a' :
                    Gamepad.press(DS4Button.CIRCLE)
                elif button == 'y':
                    Gamepad.press(DS4Button.SQUARE)
                elif button == 'x':
                    Gamepad.press(DS4Button.TRIANGLE)
                elif button == 'b':
                    Gamepad.press(DS4Button.CROSS)
                elif button == 'l':
                    Gamepad.press(DS4Button.L1)
                elif button == 'r':
                    Gamepad.press(DS4Button.R1)
                elif button == 'zl':
                    Gamepad.press(DS4Button.L2)
                elif button == 'zr':
                    Gamepad.press(DS4Button.R2)
                elif button == 'l_stick':
                    Gamepad.press(DS4Button.L3)
                elif button == 'r_stick':
                    Gamepad.press(DS4Button.R3)
                elif button == 'plus':
                    Gamepad.press(DS4Button.OPTIONS)
                elif button == 'minus':
                    Gamepad.press(DS4Button.SHARE)
                #DS4Button
            elif ch.find('release') != -1:
                stringParts = ch.split(' ')
                button = stringParts[1] # see buttonMap
                if button == 'a' :
                    Gamepad.release(DS4Button.CIRCLE)
                elif button == 'y':
                    Gamepad.release(DS4Button.SQUARE)
                elif button == 'x':
                    Gamepad.release(DS4Button.TRIANGLE)
                elif button == 'b':
                    Gamepad.release(DS4Button.CROSS)
                elif button == 'l':
                    Gamepad.release(DS4Button.L1)
                elif button == 'r':
                    Gamepad.release(DS4Button.R1)
                elif button == 'zl':
                    Gamepad.release(DS4Button.L2)
                elif button == 'zr':
                    Gamepad.release(DS4Button.R2)
                elif button == 'l_stick':
                    Gamepad.release(DS4Button.L3)
                elif button == 'r_stick':
                    Gamepad.release(DS4Button.R3)
                elif button == 'plus':
                    Gamepad.release(DS4Button.OPTIONS)
                elif button == 'minus':
                    Gamepad.release(DS4Button.SHARE)
            elif ch == '0':
                Gamepad.press(DS4Button.LOGO)
                time.sleep(1)
                Gamepad.release(DS4Button.LOGO)
    except:
        print('error >>\n')
    return ch

# Initialization
def init():
    clean_up()

def openHome():
    Gamepad.press(DS4Button.LOGO)
    time.sleep(1)
    Gamepad.release(DS4Button.LOGO)

def left_stick_right():
    Gamepad.leftXAxis(255)

def left_stick_up():
    Gamepad.leftYAxis(0)

def left_stick_left():
    Gamepad.leftXAxis(0)

def left_stick_down():
    Gamepad.leftYAxis(255)

def right_stick_right():
    Gamepad.rightXAxis(255)

def right_stick_up():
    Gamepad.rightYAxis(0)

def right_stick_left():
    Gamepad.rightXAxis(0)

def right_stick_down():
    Gamepad.rightYAxis(255)

def clean_up():
    Gamepad.releaseAll()
    Gamepad.leftXAxis(128)
    Gamepad.leftYAxis(128)
    Gamepad.rightXAxis(128)
    Gamepad.rightYAxis(128)
    Gamepad.dPad(DPad.CENTERED)

def main():
    Gamepad.begin('/dev/hidg0')

    while True:
        getch()

if __name__ == "__main__":
    main()
